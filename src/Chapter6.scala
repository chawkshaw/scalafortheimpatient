
/**
 * Created by chawkshaw on 10/05/2014.
 */
object Chapter6 extends App{
  for(i <- 1 to 5) println(Account.newUniqueNumber())
  val account = Account(200.0)
  println(account.id)
  for(colour <- TrafficLightColor.values) println(colour)
}

class Account(val id: Int, initialBalance: Double){
  private var balance = initialBalance
}
object Account{
  private var lastNumber = 0

  def apply(initialBalance: Double) =
    new Account(newUniqueNumber(), initialBalance)

  def newUniqueNumber() = {
    lastNumber += 1; lastNumber
  }
}

object TrafficLightColor extends Enumeration {
  val Red, Yellow, Green = Value
}