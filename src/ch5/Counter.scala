package ch5

/**
 * Created by chawkshaw on 06/05/2014.
 */
class Counter {
  private var value = 0
  def increment() { if(value == Int.MaxValue) () else value+=1}
  def current = value
}
