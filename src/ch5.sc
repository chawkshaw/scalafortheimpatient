class Counter{
  private var value = 0
  def increment(){ value+=1}
  def current() = value
}

val myCounter = new Counter
myCounter.increment()
println(myCounter.current())
myCounter.current



class Person{
  var age = 0
}

val aPerson = new Person
println(aPerson.age)
//aPerson.age_ = 38

import scala.beans.BeanProperty
class Person3 {
  @BeanProperty var name: String = _
}

class Person4(val name: String, val age: Int) {
  println("Just constructed another person")
  def description = name + " is " + age + " years old"
}


class Person2 {
  private var privateAge = 0 // Make private and rename
  def age = privateAge
  def age_=(newValue: Int) {
    if (newValue > privateAge) privateAge = newValue; // Can’t get younger
  }
}

val fred = new Person2
fred.age = 30
fred.age = 21
println(fred.age) // 30
