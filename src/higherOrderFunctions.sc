def sum(f: Int => Int, from:Int, to:Int):Int =
  if(from > to) 0
  else f(from) + sum(f,from+1,to)

def identity(x:Int) = x
def cube(x:Int) = x*x*x
def fact(x:Int):Int = if(x==1) 1 else x * fact(x-1)

def sumInts(from:Int, to:Int) = sum(identity, from, to)
//def sumCubes(from:Int, to:Int) = sum(cube, from, to)
def sumFact(from:Int, to:Int) = sum(fact, from, to)

def sumR(f: Int => Int)(a: Int, b: Int): Int = {
  def loop(a: Int, acc: Int): Int = {
    if (a > b) acc
    else loop(a+1, f(a) + acc)
  }
  loop(a, 0)
}

def product(f: Int => Int)(from:Int, to:Int): Int =
  if (from > to)
     1
  else f(from) * product(f) (from+1, to)

def factO(x:Int) = product (identity) (1,x)

factO(5)

def applyOnInterval(f1: (Int, Int) => Int, f2: Int => Int,endPoint:Int)(from:Int, to:Int): Int =
  if (from > to)
    endPoint
  else
    f1(f2(from),applyOnInterval(f1,f2,endPoint) (from+1,to))

def fact1(x:Int) = applyOnInterval ((z:Int, y:Int) => z*y, identity,1) (1,x)
fact1(5)
def sumCubes(from:Int, to:Int) = applyOnInterval((z:Int, y:Int) => z+y, cube, endPoint=0) (from,to)

sumCubes(1,5)
1+8+27+64+125