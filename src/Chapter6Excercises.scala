/**
 * Created by chawkshaw on 10/05/2014.
 */
object Chapter6Excercises extends App{
  println(for(arg <- args.reverse) yield print(arg + " "))
  println("a foot in centimeters is: "+ InchesToCentimeters.convert(12))
  println("a marathon in kilometers is "+ MilesToKilometers.convert(26.2))
  println("a ten gallon hat is the followin in litres: "+GallonsToLitres.convert(10))
  println(System.getProperty("file.encoding"))
  for(suit <- CardSuits.values) yield print(suit+" ")
  println()
  for(suit <- CardSuits.values) yield println(suit+" is it black? : "+CardSuits.isBlack(suit)+" is it Red? :"+CardSuits.isRed(suit))
  val point = Point(10,10)
}


object Conversions{
  def InchesToCentimeters(inches: Double) = inches * 2.54

  def GallonsToLitres(gallons: Double) = gallons * 4.54609

  def MilesToKilometers(miles: Double) = miles * 1.60934
}

abstract class UnitConversion(val conversionFactor: Double){
  def convert(unit: Double) = conversionFactor * unit
}

object InchesToCentimeters extends UnitConversion(2.54)

object GallonsToLitres extends UnitConversion(4.54609)

object MilesToKilometers extends UnitConversion(1.60934)

object Origin extends java.awt.Point{}

class Point(private val x:Int, private val y:Int)

object Point{
  def apply(x:Int, y:Int) = new Point(x,y)
}

object CardSuits extends Enumeration{
  val Clubs =  Value(9827.toChar.toString)
  val Diamonds =  Value(9830.toChar.toString)
  val Hearts =  Value(9829.toChar.toString)
  val Spades = Value(9824.toChar.toString)

  def isRed(cardSuit:Value) = !isBlack(cardSuit)
  def isBlack(cardSuit:Value) = cardSuit match {
    case Clubs | Spades  => true
    case _              => false
  }
}

object RgbColorCube extends Enumeration {
  type RgbColorCube = Value
  val Red = Value(Integer.toHexString(java.awt.Color.RED.getRGB))
  val Green = Value(Integer.toHexString(java.awt.Color.GREEN.getRGB))
  val Blue = Value(Integer.toHexString(java.awt.Color.BLUE.getRGB))
  val Cyan = Value(Integer.toHexString(java.awt.Color.CYAN.getRGB))
  val Magenta = Value(Integer.toHexString(java.awt.Color.MAGENTA.getRGB))
  val Yellow = Value(Integer.toHexString(java.awt.Color.YELLOW.getRGB))
  val Black = Value(Integer.toHexString(java.awt.Color.BLACK.getRGB))
  val White = Value(Integer.toHexString(java.awt.Color.WHITE.getRGB))
}
