

def abs(x:BigDecimal):BigDecimal = if(x<0) -x else x
def isGoodEnough(guess: BigDecimal, x: BigDecimal): Boolean =
  abs(guess * guess - x)/x < 0.001

def improveGuess(guess:BigDecimal, x:BigDecimal): BigDecimal =
  (guess + x/guess) / 2

def sqrtIter(guess: BigDecimal, x: BigDecimal): BigDecimal =
  if(isGoodEnough(guess, x)) guess
  else sqrtIter(improveGuess(guess,x),x)

def sqrt(x: BigDecimal): BigDecimal = sqrtIter(1.0, x)
sqrt(2)
sqrt(4)
sqrt(.001)
sqrt(1e-20)
sqrt(1e-6)
sqrt(1.0e20)
sqrt(1.0e50)
sqrt(1.0e60)