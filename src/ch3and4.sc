import java.awt.datatransfer._
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.Buffer

val nums = new Array[Int](10) map {(scala.math.random*100).toInt+_}


for(i <- 0 until (nums.length, 2); iplus=i+1 if(iplus<nums.length)) {
  var temp=nums(i)
  nums(i) = nums(iplus)
  nums(iplus) = temp
}


val nums2 = for(i <- 0 until nums.length; iplus=i+1;iminus=i-1) yield {
  if(iplus < nums.length && i%2==0) nums(iplus)
  else if(iplus < nums.length && i%2!=0) nums(iminus)
  else nums(i)
}

val testArray = Array(10, -21, 26, -22, 80, -75, 5, -3, 61, -13)

testArray.filter(x => x>0) ++ testArray.filter(x => x<=0)

val testArray5 = Array(10.0, -21, 26, -22, 80, -75, 5, -3, 61, -13)
testArray5.sum/testArray5.length

val testArray7 = Array(10, -3, 26, -22, 10, -75, 5, -3, 61, -13)
testArray7.toSet.toArray

// Rewrite the example at the end of Section 3.4, “Transforming Arrays,” on
// page 32. Collect indexes of the negative elements, reverse the sequence, drop
//the last index, and call a.remove(i) for each index. Compare the efficiency of
// this approach with the two approaches in Section 3.4.

java.util.TimeZone.getAvailableIDs.map(x => x.replace("America/","")).sorted
val flavors = SystemFlavorMap.getDefaultFlavorMap().asInstanceOf[SystemFlavorMap]
val monFlavour : Buffer[String] = flavors.getNativesForFlavor(DataFlavor.imageFlavor)

val goods = Map("ipad" -> 875.0, "iphone" -> 295.0, "mabook" -> 2225.0)

goods.mapValues(x=> x*.90)

var wordCount1 = Map[String, Int]()
val in1 = new java.util.Scanner(new java.io.File("/Users/chawkshaw/textFile.txt"))
while (in1.hasNext()){ var score = in1.next();wordCount1 = wordCount1 ++ Map(score -> (wordCount1.getOrElse(score,0)+1))}
for( (word,count) <- wordCount1) println(word + " : " + count)


var wordCount = scala.collection.immutable.SortedMap[String, Int]()
val in = new java.util.Scanner(new java.io.File("/Users/chawkshaw/textFile.txt"))
while (in.hasNext()){ var score = in.next();wordCount = wordCount ++ Map(score -> (wordCount.getOrElse(score,0)+1))}
for( (word,count) <- wordCount) println(word + " : " + count)

def minmax(values: Array[Int]) = (values.min, values.max)
minmax(Array(10, -21, 26, -22, 80, -75, 5, -3, 61, -13))

def lteqgt(values: Array[Int], v: Int) = (values.filter(x => x < v).length, values.filter(x => x == v).length,values.filter(x => x > v).length)

lteqgt(Array(10, -21, 26, -22, -3, -75, 5, -3, 61, -13),-3)
