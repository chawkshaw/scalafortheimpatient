import scala.math._
def isEven(n:BigInt) = {
  n % 2 == 0
}

def isPositive(n:BigInt) = {
  n > 0
}

def compute(x:BigDecimal,n:BigInt):BigDecimal = {
  if(n == 0)
    1
  else if(isEven(n) && isPositive(n)) {
    compute(x*x, n / 2)
  }
  else if(!isEven(n) && isPositive(n))
    x*compute(x,n-1)
  else
    1/compute(x,-n)
}
compute(10,2)
compute(10,3)
compute(10,5)
compute(.75,-5)
pow(.75,-5)




