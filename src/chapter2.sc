1 * 2
def signum(x: BigInt) =
  if(x > 0)
    1
  else if(x < 0)
    -1
  else
    0

signum(15)
signum(-5)
signum(1)
signum(0)
signum(-1)

{}

for(i <- 10 to(1,-1)) println(i)










def countdown(n:Int) {
  for (i <- n to(0, -1)) println(i)
}
countdown(5)








//var product = 1
//for(c <- "Hello") product*=c; product
//"Hello".product.toInt
def product(str:String) = {
  var prod:BigInt = 1
  for (c <- "Hello") prod *= c
  prod
}

def recursiveProduct(str:String):BigInt = {
  if(str.length == 0)
    0
  else if (str.length <= 1)
    1 * str.head
  else
    1 * str.head * recursiveProduct(str.tail)
}
product("Hello")
recursiveProduct("Hello")
recursiveProduct("")




